--[[
	Documentation
		Key:
			Change this to delete your entire datastore and start fresh.
		
		Version:	
			Change this to reset the data, but you can change it back to grab the old data.	
	
	The_Envelope
	September 4, 2017
--]]

local module = {
	Key = '2|4hXzO/N)t#Z9V*A8>Yz+G8&o&6',
	Version = '1'
}

return module