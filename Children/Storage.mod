--[[
	Documentation
	
	This datastore system uses tables to create, edit, and store information. You need to have some understanding of tables in order to make use of this.
	The table in line 47 is what you edit to add in starting values. From that you can edit the table by using GetData and UpdateDate.
	
	GetData
		parameters: integer (UserId)
		return: table
		purpose: Grabs a specific table that associates with the userid.
	
	UpdateData
		parameters: table (Data to update)
		return: nil
		purpose: Updates the table in storage to the edited table.
		
	SaveData
		parameters:	integer (UserId)
		return: nil
		purpose: Saves the data to the datastore. Only used when player leaves.
		
	LoadData
		parameters: integer (UserId)
		return: nil
		purpose: Loads the data from the datastore, or creates a brand new one. Only used when player joins.
		
	RemoveData
		parameters: integer (UserId)
		return: table (old data)
		purpose: Removes the data from the datastore.
		
	Any tips, comments, questions, or concerns can be directed at my twitter @GetEnveloped
	
	The_Envelope
	October 27, 2017
--]]

local storage = {}
local settings = require(script.Parent:WaitForChild("Settings"))

local DataStoreService = game:GetService("DataStoreService")
local Datastore = DataStoreService:GetDataStore(settings.Key..settings.Version)

local function createData(id)
	local player = nil
	for i,v in pairs(game.Players:GetPlayers()) do
		if v.UserId == id then
			player = v
		end
	end
	
	local tab = {
		name = player.Name,
		userId = player.UserId
	}
	
	local success, save = pcall(function()
		Datastore:SetAsync(tab.userId,tab)	
	end)
	if not success then
		warn("An error occurred with saving " ..id.. " data: "..save)
	else
		warn("Successfully created " ..id.. " data: "..game:GetService("HttpService"):JSONEncode(tab))
		end
	table.insert(storage,#storage+1,tab)
	return tab
end

local module = {
	GetData = function(id)
		local data = nil
		for i,v in pairs(storage) do
			if v.userId == id then
				data = v
			end
		end
		
		return data
	end,
	
	UpdateData = function(tab)
		for i,v in pairs(storage) do
			if v.userId == tab.userId then
				table.remove(storage,i)
			end
		end
		
		table.insert(storage,#storage+1,tab)
	end,
	
	SaveData = function(id)
		local data = nil
		print("Player leaving")
		for i,v in pairs(storage) do
			if v.userId == id then
				data = v
				table.remove(storage,i)
			end
		end
		local success, save = pcall(function()
			--Datastore:SetAsync(data.userId,data)	
			Datastore:UpdateAsync(data.userId, function(oldData)
				local newValue = data
				return newValue
			end)
		end)
		if not success then
			warn("An error occurred with saving " ..id.. " data: "..save)
		else
			warn("Successfully saved " ..id.. " data: "..game:GetService("HttpService"):JSONEncode(data))
		end
	end,
	
	LoadData = function(id)
		local data = nil
		data = Datastore:GetAsync(id)
		if data == nil then
			data = createData(id)
		else
			table.insert(storage,#storage+1,data)
		end
	end,
	
	DeleteData = function(id)
		local data = nil
		data = Datastore:RemoveAsync(id)
		
		
		return data
	end
}

return module