To view a full tutorial on this, please visit https://devforum.roblox.com/t/simple-datastore-handler-tutorial/51562

All files ending with a ".mod" are ModuleScripts.
All files ending with a ".lua" are GlobalScripts.

All files under "Children" should be a child of DatastoreHandler.lua in Roblox Studio.

Creator: GetEnveloped
Last Edited: September 6, 2017