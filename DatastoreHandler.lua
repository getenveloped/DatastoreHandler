--[[
	Documentation is in the Storage Module.
	
	Tutorial: https://devforum.roblox.com/t/simple-datastore-handler-tutorial/51562
	
	GetEnveloped
	September 6, 2017
--]]

local storage = require(script:WaitForChild("Storage"))

-- A redundant check for those who loaded before the server runs this script
for i,v in pairs(game.Players:GetPlayers()) do
	if storage.GetData(v.UserId) == nil then
		storage.LoadData(v.UserId)
	end
end

game.Players.PlayerAdded:connect(function(player)
	storage.LoadData(player.UserId)
end)

game.Players.PlayerRemoving:connect(function(player)
	storage.SaveData(player.UserId)
end)